import React from 'react';
import AppNavigation from '@app/AppNavigation';

const App = () => {
  return <AppNavigation />;
};

export default App;
