import React from 'react';
import renderer from 'react-test-renderer';
import {ShapesScreen} from '@app/screens/ShapesScreen';
import {AllShapesScreen} from '@app/screens';
import {SHAPE_SCREEN_TYPES} from '@app/config/constants';

describe('AllShapesScreen', () => {
  it('should render correct shape screen type', () => {
    const inst = renderer.create(<AllShapesScreen />);

    const shapsScreen = inst.root.findByType(ShapesScreen);
    expect(shapsScreen.props.shapesScreenType).toBe(SHAPE_SCREEN_TYPES.ALL);
  });
});
