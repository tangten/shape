import React from 'react';
import renderer from 'react-test-renderer';
import {ShapesScreen} from '@app/screens/ShapesScreen';
import {TrianglesScreen} from '@app/screens';
import {SHAPE_SCREEN_TYPES} from '@app/config/constants';

describe('TrianglesScreen', () => {
  it('should render correct shape screen type', () => {
    const inst = renderer.create(<TrianglesScreen />);

    const shapsScreen = inst.root.findByType(ShapesScreen);
    expect(shapsScreen.props.shapesScreenType).toBe(
      SHAPE_SCREEN_TYPES.TRIANGLE,
    );
  });
});
