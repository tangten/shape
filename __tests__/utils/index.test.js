import {randomColor, randomHeight, randomWidth} from '@app/utils';
import {Dimensions} from 'react-native';

const size = Dimensions.get('window');

describe('randomColor function', () => {
  it('should random correct rgb color value', () => {
    const color = randomColor();
    const [red, green, blue] = color
      .replace('rgb(', '')
      .replace(')')
      .split(',');

    expect(parseInt(red)).toBeGreaterThanOrEqual(0);
    expect(parseInt(red)).toBeLessThan(256);
    expect(parseInt(green)).toBeGreaterThanOrEqual(0);
    expect(parseInt(green)).toBeLessThan(256);
    expect(parseInt(blue)).toBeGreaterThanOrEqual(0);
    expect(parseInt(blue)).toBeLessThan(256);
  });
});

describe('randomHeight function', () => {
  it('should random height with min 10% and max 45% of device height', () => {
    const height = randomHeight();

    expect(height).toBeGreaterThanOrEqual(0.1 * size.height);
    expect(height).toBeLessThan(0.45 * size.height);
  });
});

describe('randomWidth function', () => {
  it('should random width with min 10% and max 45% of device width', () => {
    const width = randomWidth();

    expect(width).toBeGreaterThanOrEqual(0.1 * size.width);
    expect(width).toBeLessThan(0.45 * size.width);
  });
});
