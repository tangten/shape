module.exports = {
  preset: 'react-native',
  notifyMode: 'failure',
  moduleFileExtensions: ['js', 'jsx', 'json'],
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.{js,jsx}'],
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 95,
    },
  },
  coverageDirectory: '<rootDir>/coverage',
  verbose: true,
};
