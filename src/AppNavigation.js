import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  AllShapesScreen,
  CirclesScreen,
  SquaresScreen,
  TrianglesScreen,
} from '@app/screens';
import routes from '@app/config/routes';

const Tab = createBottomTabNavigator();

const AppNavigation = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen
          name={routes.squares.routeName}
          component={SquaresScreen}
          options={{
            tabBarLabel: routes.squares.label,
          }}
        />
        <Tab.Screen
          name={routes.circles.routeName}
          component={CirclesScreen}
          options={{
            tabBarLabel: routes.circles.label,
          }}
        />
        <Tab.Screen
          name={routes.triangles.routeName}
          component={TrianglesScreen}
          options={{
            tabBarLabel: routes.triangles.label,
          }}
        />
        <Tab.Screen
          name={routes.all.routeName}
          component={AllShapesScreen}
          options={{
            tabBarLabel: routes.all.label,
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigation;
