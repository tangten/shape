import React, {useState} from 'react';
import {Circle} from 'react-native-svg';
import {DOUBLE_TAB_DELAY} from '@app/config/constants';

export const CircleSvg = ({size, x, y, color, onDoubleTap}) => {
  const [lastPress, setLastPress] = useState();

  const handleOnPress = () => {
    const now = Date.now();

    setLastPress(now);
    if (now - lastPress <= DOUBLE_TAB_DELAY) {
      onDoubleTap && onDoubleTap();
    }
  };

  return (
    <Circle
      x={x}
      y={y}
      cx={size / 2}
      cy={size / 2}
      r={size / 2}
      fill={color}
      onPress={handleOnPress}
    />
  );
};

const MemoizedCircleSvg = React.memo(CircleSvg);
export default MemoizedCircleSvg;
