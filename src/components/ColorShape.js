import React, {useEffect} from 'react';
import {useRandomColor, useFetchColor} from '@app/hooks';
import {LOADING_COLOR, SHAPE_TYPES} from '@app/config/constants';
import Svg from 'react-native-svg';
import SquareSvg from './SquareSvg';
import CircleSvg from './CircleSvg';
import TriangleSvg from './TriangleSvg';

const useColorShape = () => {
  const {color: localColor, generateColor, resetColor} = useRandomColor();
  const {color: remoteColor, isError, isLoading, fetchColor} = useFetchColor();

  useEffect(() => {
    if (isError) {
      generateColor();
    } else {
      resetColor();
    }
  }, [isError]);

  useEffect(() => {
    fetchColor();
  }, []);

  const color = isLoading ? LOADING_COLOR : isError ? localColor : remoteColor;

  return {
    color,
  };
};

const SHAPE_COMPONENTS = {
  [SHAPE_TYPES.SQUARE]: SquareSvg,
  [SHAPE_TYPES.CIRCLE]: CircleSvg,
  [SHAPE_TYPES.TRIANGLE]: TriangleSvg,
};

export const ColorShape = ({size, top, left, shapeType, onDoubleTap}) => {
  const {color} = useColorShape();

  const Component = SHAPE_COMPONENTS[shapeType];
  return (
    <Component
      size={size}
      x={left}
      y={top}
      color={color}
      onDoubleTap={onDoubleTap}
    />
  );
};

const MemoizedColorShape = React.memo(ColorShape);
export default MemoizedColorShape;
