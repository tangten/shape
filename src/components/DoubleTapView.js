import React, {useState} from 'react';
import {TouchableOpacity} from 'react-native';

const DOUBLE_TAB_DELAY = 200;

export const DoubleTapView = ({children, style, onPress}) => {
  const [lastPress, setLastPress] = useState();

  const handleOnPress = () => {
    const now = Date.now();

    setLastPress(now);
    if (now - lastPress <= DOUBLE_TAB_DELAY) {
      onPress && onPress();
    }
  };

  return (
    <TouchableOpacity activeOpacity={1} style={style} onPress={handleOnPress}>
      {children}
    </TouchableOpacity>
  );
};

const MemoizedDoubleTapView = React.memo(DoubleTapView);
export default MemoizedDoubleTapView;
