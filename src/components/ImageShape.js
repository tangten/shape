import React, {useEffect, useState} from 'react';
import {useRandomColor, useFetchImage} from '@app/hooks';
import {LOADING_COLOR, SHAPE_TYPES} from '@app/config/constants';
import SquareSvg from './SquareSvg';
import CircleSvg from './CircleSvg';
import TriangleSvg from './TriangleSvg';
import ImageSvg from './ImageSvg';
import {Defs, ClipPath} from 'react-native-svg';

const useImageShape = () => {
  const [isPreloadingImage, setIsPreloadingImage] = useState(true);
  const [isPreloadingImageError, setIsPreloadingImageError] = useState(false);
  const {color: localColor, generateColor, resetColor} = useRandomColor();
  const {
    imageUrl,
    isLoading: isFetchingImage,
    isError: isFetchingImageError,
    fetchImageUrl,
  } = useFetchImage();

  useEffect(() => {
    fetchImageUrl();
  }, []);

  useEffect(() => {
    if (isFetchingImageError) {
      setIsPreloadingImage(false);
      generateColor();
    } else {
      resetColor();
    }
  }, [isFetchingImageError]);

  useEffect(() => {
    if (imageUrl) {
      fetch(imageUrl)
        .then((res) => {
          if (res.status === 200) {
            setIsPreloadingImageError(false);
          } else {
            setIsPreloadingImageError(true);
          }
        })
        .catch(() => {
          setIsPreloadingImageError(true);
        })
        .finally(() => setIsPreloadingImage(false));
    }
  }, [imageUrl]);

  const color =
    isFetchingImage || isPreloadingImage ? LOADING_COLOR : localColor;

  const isLoading = isFetchingImage || isPreloadingImage;
  const isError = isFetchingImageError || isPreloadingImageError;

  return {
    imageUrl,
    color,
    showImage: imageUrl && !isLoading && !isError,
    showColor: isLoading || isError,
  };
};

const SHAPE_COMPONENTS = {
  [SHAPE_TYPES.SQUARE]: SquareSvg,
  [SHAPE_TYPES.CIRCLE]: CircleSvg,
  [SHAPE_TYPES.TRIANGLE]: TriangleSvg,
};

export const ImageShape = ({id, size, top, left, shapeType, onDoubleTap}) => {
  const {color, imageUrl, showImage, showColor} = useImageShape();
  const Component = SHAPE_COMPONENTS[shapeType];

  return (
    <>
      {showColor && (
        <Component
          size={size}
          color={color}
          x={left}
          y={top}
          onDoubleTap={onDoubleTap}
        />
      )}
      {showImage && (
        <>
          <Defs>
            <ClipPath id={id}>
              <Component size={size} color={color} x={left} y={top} />
            </ClipPath>
          </Defs>
          <ImageSvg
            x={left}
            y={top}
            width={size}
            height={size}
            imageUrl={imageUrl}
            clipPath={`url(#${id})`}
            onDoubleTap={onDoubleTap}
          />
        </>
      )}
    </>
  );
};

const MemoizedImageShape = React.memo(ImageShape);
export default MemoizedImageShape;
