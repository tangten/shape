import React, {useState} from 'react';
import {Image} from 'react-native-svg';
import {DOUBLE_TAB_DELAY} from '@app/config/constants';

export const ImageSvg = ({size, x, y, imageUrl, clipPath, onDoubleTap}) => {
  const [lastPress, setLastPress] = useState();

  const handleOnPress = () => {
    const now = Date.now();

    setLastPress(now);
    if (now - lastPress <= DOUBLE_TAB_DELAY) {
      onDoubleTap && onDoubleTap();
    }
  };

  return (
    <Image
      x={x}
      y={y}
      width={size}
      height={size}
      preserveAspectRatio="xMidYMid slice"
      href={{uri: imageUrl}}
      clipPath={clipPath}
      onPress={handleOnPress}
    />
  );
};

const MemoizedImageSvg = React.memo(ImageSvg);
export default MemoizedImageSvg;
