import React, {useState, useCallback} from 'react';
import {StyleSheet} from 'react-native';
import {SHAPE_FILL_TYPES, SHAPE_TYPES} from '@app/config/constants';
import {randomWidth} from '@app/utils';
import ImageShape from './ImageShape';
import ColorShape from './ColorShape';

const COMPONENTS = {
  [SHAPE_FILL_TYPES.IMAGE]: ImageShape,
  [SHAPE_FILL_TYPES.COLOR]: ColorShape,
};

const getInitialFillType = (shapeType) => {
  switch (shapeType) {
    case SHAPE_TYPES.SQUARE:
      return SHAPE_FILL_TYPES.IMAGE;
    case SHAPE_TYPES.CIRCLE:
      return SHAPE_FILL_TYPES.COLOR;
    case SHAPE_TYPES.TRIANGLE:
      const random = Math.round(Math.random() * 1);

      return random ? SHAPE_FILL_TYPES.IMAGE : SHAPE_FILL_TYPES.COLOR;
    default:
      return SHAPE_FILL_TYPES.IMAGE;
  }
};

const useShape = (iTop, iLeft, iType) => {
  const [size] = useState(randomWidth());
  const [fillType, setFillType] = useState(getInitialFillType(iType));

  const switchFillType = useCallback(() => {
    if (fillType === SHAPE_FILL_TYPES.IMAGE) {
      setFillType(SHAPE_FILL_TYPES.COLOR);
    } else {
      setFillType(SHAPE_FILL_TYPES.IMAGE);
    }
  }, [fillType]);

  const pTop = iTop - size / 2;
  const pLeft = iLeft - size / 2;

  return {
    size,
    pTop,
    pLeft,
    fillType,
    switchFillType,
  };
};

const Shape = ({id, top, left, type}) => {
  const {size, pTop, pLeft, fillType, switchFillType} = useShape(
    top,
    left,
    type,
  );

  const Component = COMPONENTS[fillType];

  return (
    <Component
      id={id}
      size={size}
      shapeType={type}
      top={pTop}
      left={pLeft}
      onDoubleTap={switchFillType}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const MemoizedShape = React.memo(Shape);
export default MemoizedShape;
