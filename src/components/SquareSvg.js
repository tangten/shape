import React, {useState} from 'react';
import {Rect} from 'react-native-svg';
import {DOUBLE_TAB_DELAY} from '@app/config/constants';

export const SquareSvg = ({size, x, y, color, onDoubleTap}) => {
  const [lastPress, setLastPress] = useState();

  const handleOnPress = () => {
    const now = Date.now();

    setLastPress(now);
    if (now - lastPress <= DOUBLE_TAB_DELAY) {
      onDoubleTap && onDoubleTap();
    }
  };

  return (
    <Rect
      x={x}
      y={y}
      width={size}
      height={size}
      fill={color}
      onPress={handleOnPress}
    />
  );
};

const MemoizedSquareSvg = React.memo(SquareSvg);
export default MemoizedSquareSvg;
