import React, {useState} from 'react';
import {Polygon} from 'react-native-svg';
import {DOUBLE_TAB_DELAY} from '@app/config/constants';

export const TriangleSvg = ({size, x, y, color, onDoubleTap}) => {
  const [lastPress, setLastPress] = useState();

  const handleOnPress = () => {
    const now = Date.now();

    setLastPress(now);
    if (now - lastPress <= DOUBLE_TAB_DELAY) {
      onDoubleTap && onDoubleTap();
    }
  };

  return (
    <Polygon
      x={x}
      y={y}
      points={`${size / 2},0 ${size},${size} 0,${size}`}
      fill={color}
      onPress={handleOnPress}
    />
  );
};

const MemoizedTriangleSvg = React.memo(TriangleSvg);
export default MemoizedTriangleSvg;
