import {Dimensions} from 'react-native';

export const MAX_SIZE_PERCENT = 0.45;
export const MIN_SIZE_PERCENT = 0.1;
export const DEVICE_WIDTH = Dimensions.get('window').width;
export const DEVICE_HEIGHT = Dimensions.get('window').height;
export const SHAPE_FILL_TYPES = {
  IMAGE: 'IMAGE',
  COLOR: 'COLOR',
};
export const SHAPE_TYPES = {
  SQUARE: 'SQUARE',
  CIRCLE: 'CIRCLE',
  TRIANGLE: 'TRIANGLE',
};
export const SHAPE_SCREEN_TYPES = {
  ...SHAPE_TYPES,
  ALL: 'ALL',
};
export const LOADING_COLOR = 'rgba(196, 202, 206, 0.8)';
export const DOUBLE_TAB_DELAY = 300;
