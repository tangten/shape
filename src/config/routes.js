/* istanbul ignore file */

const routes = {
  squares: {
    routeName: 'SquaresScreen',
    label: 'Squares',
  },
  circles: {
    routeName: 'CirclesScreen',
    label: 'Circles',
  },
  triangles: {
    routeName: 'TrianglesScreen',
    label: 'Triangles',
  },
  all: {
    routeName: 'AllScreen',
    label: 'All',
  },
};

export default routes;
