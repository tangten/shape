import {useState} from 'react';
import {randomColor} from '@app/utils';

export const useRandomColor = () => {
  const [color, setColor] = useState();

  const generateColor = () => {
    setColor(randomColor());
  };

  const resetColor = () => {
    setColor(null);
  };

  return {color, generateColor, resetColor};
};

export const useFetch = (url, options = {}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [response, setResponse] = useState();

  const fetchData = () => {
    setIsLoading(true);
    setIsError(false);

    fetch(url, options)
      .then((res) => res.json())
      .then((response) => setResponse(response))
      .catch(() => setIsError(true))
      .finally(() => setIsLoading(false));
  };

  return {isLoading, isError, response, fetchData};
};

export const useFetchImage = () => {
  const {response, isError, isLoading, fetchData} = useFetch(
    'http://www.colourlovers.com/api/patterns/random?format=json',
  );
  const imageUrl = response && response.length > 0 && response[0].imageUrl;

  return {
    imageUrl,
    isError,
    isLoading,
    fetchImageUrl: fetchData,
  };
};

export const useFetchColor = () => {
  const {response, isError, isLoading, fetchData} = useFetch(
    'http://www.colourlovers.com/api/colors/random?format=json',
  );
  const colorRgb = response && response.length > 0 && response[0].rgb;
  const color = colorRgb
    ? `rgb(${colorRgb.red}, ${colorRgb.green}, ${colorRgb.blue})`
    : undefined;

  return {
    color,
    isError,
    isLoading,
    fetchColor: fetchData,
  };
};
