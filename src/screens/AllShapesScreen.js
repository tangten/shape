import React from 'react';
import ShapesScreen from './ShapesScreen';
import {SHAPE_SCREEN_TYPES} from '@app/config/constants';

const AllShapesScreen = () => {
  return <ShapesScreen shapesScreenType={SHAPE_SCREEN_TYPES.ALL} />;
};

export default AllShapesScreen;
