import React from 'react';
import ShapesScreen from './ShapesScreen';
import {SHAPE_SCREEN_TYPES} from '@app/config/constants';

const CirclesScreen = () => {
  return <ShapesScreen shapesScreenType={SHAPE_SCREEN_TYPES.CIRCLE} />;
};

export default CirclesScreen;
