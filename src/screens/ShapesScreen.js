import React, {useState} from 'react';
import {Shape} from '@app/components';
import {
  SHAPE_TYPES,
  SHAPE_SCREEN_TYPES,
  DEVICE_WIDTH,
  DEVICE_HEIGHT,
} from '@app/config/constants';
import Svg, {Rect} from 'react-native-svg';

const SHAPE_TYPES_ARRAY = [
  SHAPE_TYPES.SQUARE,
  SHAPE_TYPES.CIRCLE,
  SHAPE_TYPES.TRIANGLE,
];

export const ShapesScreen = ({shapesScreenType}) => {
  const [shapes, setShapes] = useState([]);

  const addShape = (top, left) => {
    if (shapesScreenType === SHAPE_SCREEN_TYPES.ALL) {
      const shapeTypeIndex = Math.floor(
        Math.random() * Object.entries(SHAPE_TYPES_ARRAY).length,
      );
      const shapeType = SHAPE_TYPES_ARRAY[shapeTypeIndex];

      setShapes([...shapes, {top, left, type: shapeType}]);
    } else {
      setShapes([...shapes, {top, left, type: shapesScreenType}]);
    }
  };

  return (
    <Svg
      width={DEVICE_WIDTH}
      height={DEVICE_HEIGHT}
      onPress={({nativeEvent}) =>
        addShape(nativeEvent.pageY, nativeEvent.pageX)
      }>
      <Rect x="0" y="0" width="100%" height="100%" fill="rgb(247, 247, 247)" />

      {shapes.map((shape, index) => {
        const key = `${index}_${shape.top}_${shape.left}`;
        return (
          <Shape
            key={key}
            id={key}
            top={shape.top}
            left={shape.left}
            type={shape.type}
          />
        );
      })}
    </Svg>
  );
};

const MemoizedShapesScreen = React.memo(ShapesScreen);
export default MemoizedShapesScreen;
