import React from 'react';
import ShapesScreen from './ShapesScreen';
import {SHAPE_SCREEN_TYPES} from '@app/config/constants';

const SquaresScreen = () => {
  return <ShapesScreen shapesScreenType={SHAPE_SCREEN_TYPES.SQUARE} />;
};

export default SquaresScreen;
