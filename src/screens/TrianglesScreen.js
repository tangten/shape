import React from 'react';
import ShapesScreen from './ShapesScreen';
import {SHAPE_SCREEN_TYPES} from '@app/config/constants';

const TrianglesScreen = () => {
  return <ShapesScreen shapesScreenType={SHAPE_SCREEN_TYPES.TRIANGLE} />;
};

export default TrianglesScreen;
