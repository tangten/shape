/* istanbul ignore file */

export {default as AllShapesScreen} from './AllShapesScreen';
export {default as CirclesScreen} from './CirclesScreen';
export {default as SquaresScreen} from './SquaresScreen';
export {default as TrianglesScreen} from './TrianglesScreen';
