import {
  DEVICE_HEIGHT,
  DEVICE_WIDTH,
  MIN_SIZE_PERCENT,
  MAX_SIZE_PERCENT,
} from '@app/config/constants';

export const randomHeight = () => {
  const minHeight = MIN_SIZE_PERCENT * DEVICE_HEIGHT;
  const maxHeight = MAX_SIZE_PERCENT * DEVICE_HEIGHT;

  return Math.floor(Math.random() * (maxHeight - minHeight) + minHeight);
};

export const randomWidth = () => {
  const minWidth = MIN_SIZE_PERCENT * DEVICE_WIDTH;
  const maxWidth = MAX_SIZE_PERCENT * DEVICE_WIDTH;

  return Math.floor(Math.random() * (maxWidth - minWidth) + minWidth);
};

export const randomColor = () => {
  return `rgb(
    ${Math.floor(Math.random() * 256)}, 
    ${Math.floor(Math.random() * 256)}, 
    ${Math.floor(Math.random() * 256)}
  )`;
};
